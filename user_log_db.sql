-- MySQL dump 10.13  Distrib 8.0.14, for Win64 (x86_64)
--
-- Host: localhost    Database: user_log_db
-- ------------------------------------------------------
-- Server version	8.0.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action_tb`
--

DROP TABLE IF EXISTS `action_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `action_tb` (
  `action_id` varchar(64) NOT NULL,
  `action_type_id` int(8) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `action_time` datetime DEFAULT NULL,
  PRIMARY KEY (`action_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_action_type_id` (`action_type_id`),
  CONSTRAINT `fk_action_type` FOREIGN KEY (`action_type_id`) REFERENCES `action_type_tb` (`action_type_id`),
  CONSTRAINT `fk_user_action` FOREIGN KEY (`user_id`) REFERENCES `user_tb` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_tb`
--

LOCK TABLES `action_tb` WRITE;
/*!40000 ALTER TABLE `action_tb` DISABLE KEYS */;
INSERT INTO `action_tb` VALUES ('84941053483810816',0,'ABC123XYZ','2018-10-19 03:37:28'),('84941053668360192',1,'ABC123XYZ','2018-10-19 03:37:30'),('84941053668360193',2,'ABC123XYZ','2018-10-19 03:37:30'),('84943297881047040',0,'ABC123XYZ','2018-10-19 02:37:28'),('84943297939767296',1,'ABC123XYZ','2018-10-19 02:37:30'),('84943297943961600',2,'ABC123XYZ','2018-10-19 02:37:30');
/*!40000 ALTER TABLE `action_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_type_tb`
--

DROP TABLE IF EXISTS `action_type_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `action_type_tb` (
  `action_type_id` int(8) NOT NULL,
  `action_type_name` varchar(64) NOT NULL,
  PRIMARY KEY (`action_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_type_tb`
--

LOCK TABLES `action_type_tb` WRITE;
/*!40000 ALTER TABLE `action_type_tb` DISABLE KEYS */;
INSERT INTO `action_type_tb` VALUES (0,'CLICK'),(1,'VIEW'),(2,'NAVIGATE');
/*!40000 ALTER TABLE `action_type_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `click_tb`
--

DROP TABLE IF EXISTS `click_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `click_tb` (
  `click_id` varchar(64) NOT NULL,
  `action_id` varchar(64) NOT NULL,
  `location_x` int(64) DEFAULT '0',
  `location_y` int(64) DEFAULT '0',
  PRIMARY KEY (`click_id`),
  KEY `idx_click_action` (`action_id`),
  CONSTRAINT `fk_click_action` FOREIGN KEY (`action_id`) REFERENCES `action_tb` (`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `click_tb`
--

LOCK TABLES `click_tb` WRITE;
/*!40000 ALTER TABLE `click_tb` DISABLE KEYS */;
INSERT INTO `click_tb` VALUES ('84941059053846528','84941053483810816',52,11),('84943303065206784','84943297881047040',99,99);
/*!40000 ALTER TABLE `click_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `navigate_tb`
--

DROP TABLE IF EXISTS `navigate_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `navigate_tb` (
  `navigate_id` varchar(64) NOT NULL,
  `action_id` varchar(64) NOT NULL,
  `page_from` varchar(256) DEFAULT '',
  `page_to` varchar(256) DEFAULT '',
  PRIMARY KEY (`navigate_id`),
  KEY `idx_navigate_action` (`action_id`),
  CONSTRAINT `fk_navigate_action` FOREIGN KEY (`action_id`) REFERENCES `action_tb` (`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `navigate_tb`
--

LOCK TABLES `navigate_tb` WRITE;
/*!40000 ALTER TABLE `navigate_tb` DISABLE KEYS */;
INSERT INTO `navigate_tb` VALUES ('84941059104178176','84941053668360193','communities','inventory'),('84943303098761216','84943297943961600','inventory','communities');
/*!40000 ALTER TABLE `navigate_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_tb`
--

DROP TABLE IF EXISTS `user_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_tb` (
  `user_id` varchar(64) NOT NULL,
  `session_id` varchar(64) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_tb`
--

LOCK TABLES `user_tb` WRITE;
/*!40000 ALTER TABLE `user_tb` DISABLE KEYS */;
INSERT INTO `user_tb` VALUES ('ABC123XYZ','XYZ456ABC');
/*!40000 ALTER TABLE `user_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `view_tb`
--

DROP TABLE IF EXISTS `view_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `view_tb` (
  `view_id` varchar(64) NOT NULL,
  `action_id` varchar(64) NOT NULL,
  `viewed_id` varchar(256) DEFAULT '',
  PRIMARY KEY (`view_id`),
  KEY `idx_view_action` (`action_id`),
  CONSTRAINT `fk_view_action` FOREIGN KEY (`action_id`) REFERENCES `action_tb` (`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `view_tb`
--

LOCK TABLES `view_tb` WRITE;
/*!40000 ALTER TABLE `view_tb` DISABLE KEYS */;
INSERT INTO `view_tb` VALUES ('84941059087400960','84941053668360192','FDJKLHSLD'),('84943303115538432','84943297939767296','XXXFDJKLHSLD');
/*!40000 ALTER TABLE `view_tb` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-22 15:31:36
