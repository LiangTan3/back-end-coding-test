package com.openhouse.logs.entity.vo;

/**
 * @author tan3
 * @ClassName JsonResult.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 22 - 06 : 07
 */
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @Description: Self-defined Response Type
 * 				200：Successful
 * 				400：Err, Err msg is the msg field
 */
@JsonInclude(Include.NON_NULL)
public class JsonResult {

    // Response Code
    private Integer status;

    // Response error msg
    private String error;


    // Response data
    private Object data;

    // indicate successful response or not
    private String success;


    public static JsonResult success(Object data) {
        return new JsonResult(200, null, data, "true");
    }

    public static JsonResult success() {
        return new JsonResult(200, null, null, "true");
    }

    public static JsonResult successWithDataOnly(Object data) {
        return new JsonResult(200, null, data, null);
    }

    public static JsonResult errorMsg(String error) {
        return new JsonResult(400, error, null, null);
    }

    public JsonResult() {

    }


    public JsonResult(Integer status, String error, Object data, String success) {
        this.status = status;
        this.error = error;
        this.data = data;
        this.success = success;
    }


    public Boolean isSuccess() {
        return this.status == 200;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
