package com.openhouse.logs.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author tan3
 * @ClassName Navigate.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 19 : 09
 * CREATE TABLE `navigate_tb` (
 * 	   `navigate_id` varchar(64) NOT NULL,
 *     `action_id` varchar(64) NOT NULL,
 *     `page_from` varchar(256) DEFAULT '',
 *     `page_to` varchar(256) DEFAULT '',
 * 	   PRIMARY KEY `pri_navigate_id`(`navigate_id`),
 *     KEY `idx_navigate_action` (`action_id`),
 * 	   CONSTRAINT `fk_navigate_action` FOREIGN KEY (`action_id`) REFERENCES `action_tb` (`action_id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Navigate extends Property{

    private String navigateId;

    private Action action;

    private String pageFrom;

    private String pageTo;
}
