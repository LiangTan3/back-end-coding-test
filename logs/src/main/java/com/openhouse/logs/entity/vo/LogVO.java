package com.openhouse.logs.entity.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tan3
 * @ClassName LogVO.java
 * @Description Convert the log JSON object to Log View Object.
 * @createTime 2020 -  08 - 22 - 06 : 11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogVO {

    private String userId;
    private String sessionId;

    @JsonProperty(value = "actions")
    private List<ActionVO> actionVOS;
}
