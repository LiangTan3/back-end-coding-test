package com.openhouse.logs.entity.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.openhouse.logs.entity.Action;
import com.openhouse.logs.entity.Click;
import com.openhouse.logs.entity.Navigate;
import com.openhouse.logs.entity.Property;
import com.openhouse.logs.entity.View;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tan3
 * @ClassName ActionVO.java
 * @Description The Action View Object converted by JSON property actions,
 * Property has different type, provide a unified interface called property and dynamically deserialization
 * using java reflection.
 * @createTime 2020 -  08 - 22 - 06 : 15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ActionVO {

    private String time; // time str with ISO 8601 format

    private String type;

    @JsonProperty(value = "properties")
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", include = JsonTypeInfo.As.EXTERNAL_PROPERTY)
    @JsonSubTypes(value = {
        @JsonSubTypes.Type(value = Click.class, name = "CLICK"),
        @JsonSubTypes.Type(value = View.class, name = "VIEW"),
        @JsonSubTypes.Type(value = Navigate.class, name = "NAVIGATE")
    })
    private Property property;
}
