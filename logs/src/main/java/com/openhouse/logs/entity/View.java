package com.openhouse.logs.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author tan3
 * @ClassName View.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 19 : 11
 *
 * # DROP TABLE IF EXISTS `view_tb`;
 * CREATE TABLE `view_tb` (
 * 	   `view_id` varchar(64) NOT NULL,
 *     `action_id` varchar(64) NOT NULL,
 *     `viewed_id` varchar(256) DEFAULT '',
 * 	   PRIMARY KEY `pri_view_id`(`view_id`),
 *     KEY `idx_view_action` (`action_id`),
 * 	   CONSTRAINT `fk_view_action` FOREIGN KEY (`action_id`) REFERENCES `action_tb` (`action_id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class View extends Property{
    private String viewId;
    private String viewedId;
    private Action action;
}
