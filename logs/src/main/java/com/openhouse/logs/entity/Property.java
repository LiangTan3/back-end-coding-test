package com.openhouse.logs.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author tan3
 * @ClassName Property.java
 * @Description Provide unified interface for all types of properties (for the purpose of deserialization)
 * @createTime 2020 -  08 - 22 - 06 : 16
 */
@JsonInclude(Include.NON_NULL)
public class Property {
    private Action action;

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
