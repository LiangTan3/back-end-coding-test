package com.openhouse.logs.entity;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tan3
 * @ClassName Action.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 18 : 45
 * CREATE TABLE `action_tb` (
 * 	   `action_id` varchar(64) NOT NULL,
 *     `action_type_id` int(8) NOT NULL,
 *     `user_id` varchar(64) NOT NULL,
 *     `action_time` DATETIME DEFAULT NULL,
 *     PRIMARY KEY `pri_action_id`(`action_id`),
 *     KEY `idx_user_id` (`user_id`),
 *     KEY `idx_action_type_id` (`action_type_id`),
 *     CONSTRAINT `fk_user_action` FOREIGN KEY (`user_id`) REFERENCES `user_tb` (`user_id`),
 *     CONSTRAINT `fk_action_type` FOREIGN KEY (`action_type_id`) REFERENCES `action_type_tb` (action_type_id)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Action {

    private String actionId;
    private ActionType actionType;
    private User user;
    private Date actionTime;
}
