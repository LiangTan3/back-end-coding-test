package com.openhouse.logs.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author tan3
 * @ClassName Click.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 18 : 51
 *
 * CREATE TABLE `click_tb` (
 * 	   `click_id` varchar(64) NOT NULL,
 *     `action_id`  varchar(64) NOT NULL,
 *     `location_x` int(64) DEFAULT 0,
 *     `location_y` int(64) DEFAULT 0,
 * 	   PRIMARY KEY `pri_click_id`(`click_id`),
 *     KEY `idx_click_action` (`action_id`),
 * 	   CONSTRAINT `fk_click_action` FOREIGN KEY (`action_id`) REFERENCES `action_tb` (`action_id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Click extends Property{
    private String clickId;

    private Action action;

    private int locationX;

    private int locationY;

}
