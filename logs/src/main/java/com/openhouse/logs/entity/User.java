package com.openhouse.logs.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tan3
 * @ClassName User.java
 * @Description User Entity Class
 * align with user_tb;
 * CREATE TABLE `user_tb` (
 * 	`user_id` varchar(64) NOT NULL,
 *     `session_id` varchar(64) NOT NULL,
 *      PRIMARY KEY `pri_use_id`(`user_id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
 * @createTime 2020 -  08 - 21 - 18 : 42
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
    private String userId;
    private String sessionId;
}
