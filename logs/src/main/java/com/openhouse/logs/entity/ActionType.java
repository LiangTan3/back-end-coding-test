package com.openhouse.logs.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tan3
 * @ClassName ActionType.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 18 : 48
 * CREATE TABLE `action_type_tb` (
 * 	   `action_type_id` int(8) NOT NULL,
 *     `action_type_name` varchar(64) NOT NULL,
 *     PRIMARY KEY `pri_action_type_id`(`action_type_id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ActionType {

    private int actionTypeId;

    private String actionTypeName; // should be all in Capital Letter;
}
