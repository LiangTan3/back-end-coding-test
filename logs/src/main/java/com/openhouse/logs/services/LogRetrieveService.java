package com.openhouse.logs.services;

import com.openhouse.logs.entity.ActionType;
import com.openhouse.logs.entity.vo.ActionVO;
import com.openhouse.logs.exceptions.LogServiceException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author tan3
 * @ClassName LogRetrieveService.java
 * @Description Query The logs by the Different conditions,
 * This can be done in parallel,
 * FOR MsSQL InnoDB engine,
 * When a user holds a READ LOCK on a table, other users can also read or hold a READ LOCK,
 * but no user can write or hold a WRITE LOCK on that table.
 * Since we will usually have Master_slave architecture, we only read on slave databases;
 * Map<String, ActionVO>, the key is the actionId, the value is the view object for action.
 * @createTime 2020 -  08 - 22 - 11 : 22
 */
public interface LogRetrieveService {

    CompletableFuture<Map<String, ActionVO>> fetchLogByUserId(String userId)  throws LogServiceException;

    CompletableFuture<Map<String, ActionVO>> fetchLogByTimeRange(Date dateFrom, Date dateTo)  throws LogServiceException;

    CompletableFuture<Map<String, ActionVO>> fetchLogByLogType(ActionType actionType) throws LogServiceException;
}
