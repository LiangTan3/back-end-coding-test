package com.openhouse.logs.services;

import com.openhouse.logs.entity.Property;
import com.openhouse.logs.entity.User;
import com.openhouse.logs.entity.Action;
import com.openhouse.logs.exceptions.LogServiceException;
import java.util.List;
import java.util.Map;

/**
 * @author tan3
 * @ClassName LogStorageService.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 22 - 08 : 47
 */
public interface LogStorageService {

    /**
     * Log Storage into the databases.
     * @param user
     * @param actionList
     * @param propertyMap
     * @return
     */
    int storeUserLog(User user,List<Action> actionList,
        Map<Class, List<Property>> propertyMap) throws LogServiceException;

}
