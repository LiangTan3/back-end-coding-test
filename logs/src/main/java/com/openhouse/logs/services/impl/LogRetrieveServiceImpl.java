package com.openhouse.logs.services.impl;

import com.openhouse.logs.entity.ActionType;
import com.openhouse.logs.entity.Property;
import com.openhouse.logs.entity.vo.ActionVO;
import com.openhouse.logs.enums.ActionTypeEnum;
import com.openhouse.logs.exceptions.LogServiceException;
import com.openhouse.logs.repository.ActionRepository;
import com.openhouse.logs.repository.ClickRepository;
import com.openhouse.logs.repository.NavigateRepository;
import com.openhouse.logs.repository.ViewRepository;
import com.openhouse.logs.services.LogRetrieveService;
import com.openhouse.logs.utils.TimeUtil;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import com.openhouse.logs.entity.Action;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author tan3
 * @ClassName UserServiceImpl.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 22 - 11 : 26
 */
@Service
public class LogRetrieveServiceImpl implements LogRetrieveService {


    @Autowired
    private ActionRepository actionRepository;
    @Autowired
    private NavigateRepository navigateRepository;
    @Autowired
    private ViewRepository viewRepository;
    @Autowired
    private ClickRepository clickRepository;

    @Async
    @Override
    public CompletableFuture<Map<String, ActionVO>> fetchLogByUserId(String userId)  throws LogServiceException {
        List<Action> actionList = null;

        try {
            actionList = actionRepository.queryActionsByUserId(userId);
        } catch (Exception e) {
           throw new LogServiceException("action query error [query by userId],  "
               + " details: \n" + e.getMessage());
        }
        Map<String, ActionVO> actionVOMap = null;
        try {
            actionVOMap = queryPropertiesAndSetVO(actionList);
        } catch (Exception e) {
            throw new LogServiceException("Log Service searching exception: \n" + e.getMessage());
        }

        return CompletableFuture.completedFuture(actionVOMap);
    }

    @Async
    @Override
    public CompletableFuture<Map<String, ActionVO>> fetchLogByTimeRange(Date dateFrom, Date dateTo)
        throws LogServiceException {
        List<Action> actionList = null;

        try {
            actionList = actionRepository.queryActionsByTimeRange(dateFrom, dateTo);
        } catch (Exception e) {
            throw new LogServiceException("action query error[query by time range] "
                + " details: \n" + e.getMessage());
        }
        Map<String, ActionVO> actionVOMap = null;
        try {
            actionVOMap = queryPropertiesAndSetVO(actionList);
        } catch (Exception e) {
            throw new LogServiceException("Log Service searching exception: \n" + e.getMessage());
        }
        return CompletableFuture.completedFuture(actionVOMap);
    }

    @Async
    @Override
    public CompletableFuture<Map<String, ActionVO>> fetchLogByLogType(ActionType actionType)
        throws LogServiceException {
        List<Action> actionList = null;

        try {
            actionList = actionRepository.queryActionsByActionTypeId(actionType.getActionTypeId());
        } catch (Exception e) {
            throw new LogServiceException("action query error[query by time range], "
                + " details: \n" + e.getMessage());
        }
        Map<String, ActionVO> actionVOMap = null;
        try {
            actionVOMap = queryPropertiesAndSetVO(actionList);
        } catch (Exception e) {
            throw new LogServiceException("Log Service searching exception: \n" + e.getMessage());
        }
        return CompletableFuture.completedFuture(actionVOMap);
    }


    private Map<String, ActionVO> queryPropertiesAndSetVO(List<Action> actionList) {
        Map<String, ActionVO> actionVOMap = new HashMap<>();
        for (int idx = 0; idx < actionList.size(); idx++) {
            Action action = actionList.get(idx);
            String id = action.getActionId();
            ActionVO actionVO = new ActionVO();

            actionVO.setTime(TimeUtil.convertDateToString(action.getActionTime()));
            actionVO.setType(action.getActionType().getActionTypeName());

            int actionTypeId = action.getActionType().getActionTypeId();
            Property property = null;
            try {
                if (ActionTypeEnum.CLICK.getActionTypeId() == actionTypeId) {
                    property = clickRepository.queryClicksByActionId(id);
                } else if (ActionTypeEnum.VIEW.getActionTypeId() == actionTypeId) {
                    property = viewRepository.queryViewByActionId(id);
                } else if (ActionTypeEnum.NAVIGATE.getActionTypeId() == actionTypeId) {
                    property = navigateRepository.queryNavigateByActionId(id);
                }
            } catch (Exception e) {
                throw new LogServiceException("Log Service searching exception: \n" + e.getMessage());
            }
            actionVO.setProperty(property);
            actionVOMap.put(id, actionVO);
        }
        return actionVOMap;
    }
}
