package com.openhouse.logs.services.impl;

import com.openhouse.logs.entity.Action;
import com.openhouse.logs.entity.Click;
import com.openhouse.logs.entity.Navigate;
import com.openhouse.logs.entity.Property;
import com.openhouse.logs.entity.User;
import com.openhouse.logs.entity.View;
import com.openhouse.logs.exceptions.LogServiceException;
import com.openhouse.logs.repository.ActionRepository;
import com.openhouse.logs.repository.ClickRepository;
import com.openhouse.logs.repository.NavigateRepository;
import com.openhouse.logs.repository.UserRepository;
import com.openhouse.logs.repository.ViewRepository;
import com.openhouse.logs.services.LogStorageService;
import com.openhouse.logs.utils.SnowflakeIdWorker;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author tan3
 * @ClassName LogStorageServiceImpl.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 22 - 08 : 52
 */
@Service
public class LogStorageServiceImpl implements LogStorageService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ActionRepository actionRepository;

    @Autowired
    private NavigateRepository navigateRepository;
    @Autowired
    private ViewRepository viewRepository;
    @Autowired
    private ClickRepository clickRepository;

    /**
     * Store logs information to the database, must open a new transaction
     * if the caller has not already started a transaction.
     * Rolled back if an error occurs.
     * Step 1: Insert User information (if user already exists in the database, skipp this step and continue.
     * Step 2: Insert Action information
     * Step 3: (Paralleled) insert actions based on their Type.
     * @param user
     * @param actionList
     * @param propertyMap
     * @return
     * @throws LogServiceException
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public int storeUserLog(User user, List<Action> actionList,
        Map<Class, List<Property>> propertyMap) throws LogServiceException {
        // Step 1: Insert User information (if not in the database)
        User queryUser = null;
        try {
            queryUser = userRepository.queryUserByUserId(user.getUserId());
        } catch (Exception e) {
            throw new LogServiceException("user [query error]");
        }

        if (queryUser == null  || !queryUser.getUserId().equals(user.getUserId())) {
            try {
                int affectedRow = userRepository.insertUser(user);
                if (affectedRow == -1) {
                    throw new LogServiceException("user [insertion error]");
                }
            } catch (Exception e ){
                throw new LogServiceException("user [insertion error]");
            }
        }

        // Step 2: Insert Action information
        try {
            int affectedRow = actionRepository.batchInsertActions(actionList);
            if (affectedRow == -1) {
                throw new LogServiceException("actions [insertion error]");
            }
        } catch (Exception e ){
            throw new LogServiceException("actions [insertion error]");
        }

        // Step 3: (Paralleled) insert actions based on their Type.
        List<CompletableFuture<Integer>> futures = new ArrayList<>();
        for (Class cls: propertyMap.keySet()) {
            if (cls == Click.class) {
                List<Click> clickList= (List<Click>)(List<?>)propertyMap.get(cls);
                futures.add(this.insertClicksAsync(clickList));
            } else if (cls == View.class) {
                List<View> viewList= (List<View>)(List<?>)propertyMap.get(cls);
                futures.add(this.insertViewsAsync(viewList));
            } else if (cls == Navigate.class) {
                List<Navigate> navigateList= (List<Navigate>)(List<?>)propertyMap.get(cls);
                futures.add(this.insertNavigatesAsync(navigateList));
            }
        }

        CompletableFuture[] cfs = futures.toArray(new CompletableFuture[futures.size()]);
        // combination of all completable future and wait until all of them are finished
        try {
            CompletableFuture allof = CompletableFuture.allOf(cfs);
            allof.join();
        } catch (Exception e) {
            throw new LogServiceException("insertion error:  details \n" + e.getMessage());
        }

        return 0;
    }

    @Async
    public CompletableFuture<Integer> insertClicksAsync(List<Click> clicks) throws LogServiceException{
        SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0, 0);
        for (Click click: clicks ) {
            click.setClickId(String.valueOf(idWorker.nextId()));
        }
        try {
            int affectedRow = clickRepository.batchInsertClick(clicks);
            if (affectedRow == -1) {
                throw new LogServiceException("clicks [insertion error]");
            }
            return CompletableFuture.completedFuture(affectedRow);
        } catch (Exception e ){
            throw new LogServiceException("clicks [insertion error]: " + e.getMessage());
        }
    }

    @Async
    public CompletableFuture<Integer> insertViewsAsync(List<View> views) throws LogServiceException{
        SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0, 0);
        for (View view: views ) {
            view.setViewId(String.valueOf(idWorker.nextId()));
        }
        try {
            int affectedRow = viewRepository.batchInsertView(views);
            if (affectedRow == -1) {
                throw new LogServiceException("views [insertion error]");
            }
            return CompletableFuture.completedFuture(affectedRow);
        } catch (Exception e ){
            throw new LogServiceException("views [insertion error] : " + e.getMessage());
        }
    }
    @Async
    public CompletableFuture<Integer> insertNavigatesAsync(List<Navigate> navs) throws LogServiceException{
        SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0, 0);
        for (Navigate nav: navs ) {
            nav.setNavigateId(String.valueOf(idWorker.nextId()));
        }
        try {
            int affectedRow = navigateRepository.batchInsertNavigate(navs);
            if (affectedRow == -1) {
                throw new LogServiceException("navigates [insertion error]");
            }
            return CompletableFuture.completedFuture(affectedRow);
        } catch (Exception e ){
            throw new LogServiceException("navigates [insertion error]: " + e.getMessage());
        }
    }
}
