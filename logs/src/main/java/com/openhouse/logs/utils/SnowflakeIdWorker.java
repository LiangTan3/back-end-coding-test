package com.openhouse.logs.utils;
/**
 * @author tan3
 * @ClassName SnowflakeIdWorker.java
 * @Description
 *
 * Twitter_Snowflake <br>
 * SnowFlake snowflake id structure(in bit representation), each part is separated by spaces <br>
 * 0 - 0000000000 0000000000 0000000000 0000000000 0 - 00000 - 00000 - 000000000000 <br>
 * 1 bit signature，long type in java usually has sign, id numbers are always positive, <br>
 * 41 bits timestamp (in milliseconds)，
 * Notice that the timestamp here represents the difference between currentTime - startTime
 * The starting timestamp is set by the algorithm with startTime property,
 * e.g 41 bits long timestamp can use for 69 years
 * Year = (1 << 41) / (1000L * 60 * 60 * 24 * 365) = 69<br>
 * 10bits worker id, can be deployed to 1024 nodes ，with 5 bits of data center id, and 5 bits worker id<br>
 * 12bits sequence number ，counting in millisecond，12 bits can support 4096 id  for the same worker at the same timestamp
 * <br>
 * Return a Long Type Generated Id <br>
 * SnowFlake algorithm can be used in
 *
 * @createTime 2020 -  08 - 21 - 21 : 26
 */
public class SnowflakeIdWorker {

    // ==============================Fields===========================================
    /** Starting time stamp (2020-01-01 0:0:00) */
    private final long twepoch = 1577862000000L;

    /** work Id bits length  */
    private final long workerIdBits = 5L;

    /** data center id bis length */
    private final long datacenterIdBits = 5L;

    /** maxWorkerId，31 if the length is 5 bits long*/
    private final long maxWorkerId = -1L ^ (-1L << workerIdBits);

    /** max Data Center Id */
    private final long maxDatacenterId = -1L ^ (-1L << datacenterIdBits);

    /** Sequence length */
    private final long sequenceBits = 12L;

    /** work Id shift length */
    private final long workerIdShift = sequenceBits;

    /** data center id shift length(12+5) */
    private final long datacenterIdShift = sequenceBits + workerIdBits;

    /** time stamp shift length */
    private final long timestampLeftShift = sequenceBits + workerIdBits + datacenterIdBits;

    /** sequence Mask，in this case equal to 4095 (0b111111111111=0xfff=4095) */
    private final long sequenceMask = -1L ^ (-1L << sequenceBits);

    /** worker id (0~31) */
    private long workerId;

    /** data center id (0~31) */
    private long datacenterId;

    /** sequence number (0~4095) */
    private long sequence = 0L;

    /** last time generating id timestamp */
    private long lastTimestamp = -1L;

    //==============================Constructors=====================================
    /**
     * Create id generator with workder id and data center id
     * @param workerId (0~31)
     * @param datacenterId (0~31)
     */
    public SnowflakeIdWorker(long workerId, long datacenterId) {
        if (workerId > maxWorkerId || workerId < 0) {
            throw new IllegalArgumentException(String.format("worker Id can't be greater than %d or less than 0", maxWorkerId));
        }
        if (datacenterId > maxDatacenterId || datacenterId < 0) {
            throw new IllegalArgumentException(String.format("data center Id can't be greater than %d or less than 0", maxDatacenterId));
        }
        this.workerId = workerId;
        this.datacenterId = datacenterId;
    }

    // ==============================Methods==========================================
    /**
     * Get next id  (Thread-safe)
     * @return SnowflakeId
     *
     **/
    public synchronized long nextId() {
        long timestamp = timeGen();

        // if the timestamp is smaller than the lastTimestamp throw a exception
        if (timestamp < lastTimestamp) {
            throw new RuntimeException(
                String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
        }

        //if happens in the same stamp, increase the sequence number.
        if (lastTimestamp == timestamp) {
            sequence = (sequence + 1) & sequenceMask;
            //if sequence is out of range
            if (sequence == 0) {
                //block and wait for next timestamp
                timestamp = tilNextMillis(lastTimestamp);
            }
        } else {
            //reset the sequence number when timestamp increases
            sequence = 0L;
        }

        //update lastTimeStamp
        lastTimestamp = timestamp;

        //Combine all bits adn return the generated id.
        return ((timestamp - twepoch) << timestampLeftShift) //
            | (datacenterId << datacenterIdShift) //
            | (workerId << workerIdShift) //
            | sequence;
    }

    /**
     * wait until the next timestamp.
     * @param lastTimestamp
     * @return currentTimestamp
     */
    protected long tilNextMillis(long lastTimestamp) {
        long timestamp = timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = timeGen();
        }
        return timestamp;
    }

    /**
     * return currentTimeMilliseconds
     * @return currentTime(milliseconds)
     */
    protected long timeGen() {
        return System.currentTimeMillis();
    }

    //==============================Test=============================================
    /*public static void main(String[] args) {
        SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0, 0);
        for (int i = 0; i < 1000; i++) {
            long id = idWorker.nextId();
            System.out.println(Long.toBinaryString(id));
            System.out.println(id);
        }
    }*/
}
