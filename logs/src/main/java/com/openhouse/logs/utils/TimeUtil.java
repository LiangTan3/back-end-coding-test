package com.openhouse.logs.utils;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Date;

/**
 * @author tan3
 * @ClassName TimeUtil.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 19 : 28
 */
public class TimeUtil {

    // default is mountain time
    private static final ZoneId DEFAULT_ZONE_ID = ZoneId.of( "US/Mountain" ) ;
    /**
     *
     * Parse the time Str to Date Object with the same time zone
     * @param timeStr ISO 8601
     * The input string format is defined in the ISO 8601 standard,
     * a family of date-time formats.
     * @return
     */
    public static Date parseOffsetTimeStr(String timeStr) throws DateTimeParseException {
        OffsetDateTime odt = OffsetDateTime.parse(timeStr);
        long epochMilli = odt.toInstant().toEpochMilli();
        return new Date(epochMilli);
    }

    public static String convertDateToString(Date date) {
        Instant instant = date.toInstant();
        ZonedDateTime zdt = instant.atZone(DEFAULT_ZONE_ID) ;
        String zdtStr = zdt.toString();
        String pattern = "\\[.*?\\]";
        return zdtStr.replaceAll(pattern, "");
    }

    public static void main(String[] args) {
        String timeStr = "2018-10-18T21:37:28-06:00"; // "2018-10-18T21:37:28-06:00";
        Date date = TimeUtil.parseOffsetTimeStr(timeStr);
        // System.out.println(convertDateToString(date));
    }
}
