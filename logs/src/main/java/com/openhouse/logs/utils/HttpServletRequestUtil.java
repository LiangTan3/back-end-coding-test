package com.openhouse.logs.utils;

import com.openhouse.logs.entity.ActionType;
import com.openhouse.logs.enums.ActionTypeEnum;
import com.openhouse.logs.exceptions.InvalidInputException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;

/**
 * @author tan3
 * @ClassName HttpServletRequestUtil.java
 * @Description Use NULL Object Design pattern, if the query parameter does not represent in the REST URL
 * then return null.
 * @createTime 2020 -  08 - 22 - 10 : 54
 */
public class HttpServletRequestUtil {

    public static String getUserIdFromHttpRequest (HttpServletRequest request) {
        String userId = request.getParameter("userId");
        if (StringUtils.isBlank(userId)){
            return null;
        }
        return userId;
    }

    /**
     * The query time range should be in ISO-8601 format.
     * @param request
     * @return
     * @throws InvalidInputException
     */
    public static Date[] getTimeRange (HttpServletRequest request) throws InvalidInputException {
        String timeFromStr = request.getParameter("timeFrom");
        String timeToStr = request.getParameter("timeTo");
        if (StringUtils.isEmpty(timeFromStr) && StringUtils.isEmpty(timeToStr)) {
            return null;
        }
        if (StringUtils.isEmpty(timeFromStr)) {
            throw new InvalidInputException("missing timeTo parameter");
        }
        if (StringUtils.isEmpty(timeToStr)) {
            throw new InvalidInputException("missing timeFrom parameter");
        }
        Date[] dates = new Date[2];
        // dateFrom and dateTo should all presents in the url parameters
        try {
            Date dateFrom = TimeUtil.parseOffsetTimeStr(timeFromStr);
            Date dateTo = TimeUtil.parseOffsetTimeStr(timeToStr);
            dates[0] = dateFrom;
            dates[1] = dateTo;
        } catch (Exception e) {
            throw new InvalidInputException("action [action time format] is invalid, details: \n "
                + e.getMessage());
        }
        return dates;
    }

    /**
     * Return the ActionType Object given the logType
     * @param request
     * @return
     * @throws InvalidInputException
     */
    public static ActionType getActionTypeEnum(HttpServletRequest request) {
        String logType = request.getParameter("logType");
        if (StringUtils.isEmpty(logType)){
            return null;
        }

        int actionTypeId = ActionTypeEnum.getActionTypeEnumIdByName(logType);

        if (actionTypeId == -1) {
            throw new InvalidInputException("logType  is invalid");
        }

        return ActionType.builder()
            .actionTypeId(actionTypeId)
            .actionTypeName(logType)
            .build();
    }
}
