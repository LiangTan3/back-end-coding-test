package com.openhouse.logs.utils;

import com.openhouse.logs.entity.ActionType;
import com.openhouse.logs.entity.Property;
import com.openhouse.logs.entity.User;
import com.openhouse.logs.entity.vo.ActionVO;
import com.openhouse.logs.entity.vo.LogVO;
import com.openhouse.logs.entity.Action;
import com.openhouse.logs.enums.ActionTypeEnum;
import com.openhouse.logs.exceptions.InvalidInputException;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

/**
 * @author tan3
 * @ClassName ObjectParserUtil.java
 * @Description This is the utility class to parse ViewObject from the Front-end json object.
 * @createTime 2020 -  08 - 22 - 07 : 28
 */
public class ObjectParserUtil {

    private static SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0, 0);


    public static User getUserFromLogVO(LogVO logVO) throws InvalidInputException{
        String userId = logVO.getUserId();
        String sessionId = logVO.getSessionId();
        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(sessionId)) {
            throw new InvalidInputException("user [userId | sessionId] is missing ");
        }
        User user = User.builder().userId(userId).sessionId(sessionId).build();
        return user;
    }


    public static List<Action> getActionsFromLogVO(LogVO logVO) throws InvalidInputException {
        List<Action> actionList = new ArrayList<>();
        List<ActionVO> actionVOList = logVO.getActionVOS();

        if (actionVOList == null || actionVOList.isEmpty()) {
            return actionList;
        }
        User user = getUserFromLogVO(logVO);
        for (ActionVO actionVO : actionVOList) {
            String actionId = String.valueOf(idWorker.nextId());
            String actionTimeStr = actionVO.getTime();
            Date actionTime = null;
            try {
                actionTime = TimeUtil.parseOffsetTimeStr(actionTimeStr);
            }catch (DateTimeParseException exception) {
                throw new InvalidInputException("action [action time format] is invalid, details: \n "
                    + exception.getMessage());
            }
            String actionTypeName = actionVO.getType();
            int actionTypeId = ActionTypeEnum.getActionTypeEnumIdByName(actionTypeName);
            if (actionTypeId == - 1) {
                throw new InvalidInputException("action [action type ] is invalid ");
            }
            ActionType actionType = ActionType.builder()
                .actionTypeId(actionTypeId)
                .actionTypeName(actionTypeName)
                .build();
            Action action = Action.builder()
                .actionId(actionId)
                .user(user)
                .actionTime(actionTime)
                .actionType(actionType)
                .build();
            actionList.add(action);
        }
        return actionList;
    }

    /**
     * group properties base on their class information.
     * @param logVO
     * @return
     */
    public static Map<Class, List<Property>> getpropertyMapFromLogVO(LogVO logVO, List<Action> actionList)
        throws InvalidInputException {
        List<ActionVO>  actionVOList = logVO.getActionVOS();
        Set<Class> typeSet = ActionTypeEnum.getAllActionTypeClass();
        Map<Class, List<Property>> propertyMap= new HashMap<>();
        for (int idx = 0; idx < actionVOList.size(); idx++) {
            ActionVO actionVO = actionVOList.get(idx);
            Property property = actionVO.getProperty();
            property.setAction(actionList.get(idx));
            if (!typeSet.contains(property.getClass())) {
                throw new InvalidInputException("property [property type] invalid");
            }
            propertyMap.putIfAbsent(property.getClass(), new ArrayList<>());
            propertyMap.get(property.getClass()).add(property);
        }
        return propertyMap;
    }
}
