package com.openhouse.logs.repository;

import com.openhouse.logs.entity.View;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * @author tan3
 * @ClassName ViewRepository.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 19 : 14
 */
@Repository
public interface ViewRepository {
    int batchInsertView(List<View> clicks);

    View queryViewByActionId(String actionId);

    int deleteViewByID(String viewId);
}
