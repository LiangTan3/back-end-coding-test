package com.openhouse.logs.repository;

import com.openhouse.logs.entity.Navigate;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * @author tan3
 * @ClassName NavigateRepository.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 19 : 13
 */
@Repository
public interface NavigateRepository {
    int batchInsertNavigate(List<Navigate> navigates);
    Navigate queryNavigateByActionId(String actionId);
    int deleteNavigateByID(String navigateId);
}
