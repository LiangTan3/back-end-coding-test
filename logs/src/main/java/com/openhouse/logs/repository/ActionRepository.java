package com.openhouse.logs.repository;

import com.openhouse.logs.entity.Action;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author tan3
 * @ClassName ActionRepository.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 19 : 12
 */
@Repository
public interface ActionRepository {

    int batchInsertActions(List<Action> actions);
    List<Action> queryActionsByUserId(String userId);
    List<Action> queryActionsByActionTypeId(int actionTypeId);
    List<Action> queryActionsByTimeRange(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);
    int deleteActionByActionId(String actionId);

}
