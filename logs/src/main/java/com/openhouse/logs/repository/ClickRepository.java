package com.openhouse.logs.repository;

import com.openhouse.logs.entity.Click;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * @author tan3
 * @ClassName ClickRepository.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 19 : 13
 */
@Repository
public interface ClickRepository {

    int batchInsertClick(List<Click> clicks);

    Click queryClicksByActionId(String actionId);

    int deleteClickByID(String clickId);
}
