package com.openhouse.logs.repository;

import com.openhouse.logs.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author tan3
 * @ClassName UserRepository.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 19 : 12
 */
@Repository
public interface UserRepository {


    int insertUser(User user);

    User queryUserByUserId(String userId);

    int deleteUserByUserId(String userId);
}
