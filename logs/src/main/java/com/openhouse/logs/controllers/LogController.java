package com.openhouse.logs.controllers;

import com.openhouse.logs.entity.ActionType;
import com.openhouse.logs.entity.vo.ActionVO;
import com.openhouse.logs.entity.vo.JsonResult;
import com.openhouse.logs.entity.Property;
import com.openhouse.logs.entity.User;
import com.openhouse.logs.entity.vo.LogVO;
import com.openhouse.logs.exceptions.InvalidInputException;
import com.openhouse.logs.exceptions.LogServiceException;
import com.openhouse.logs.services.LogRetrieveService;
import com.openhouse.logs.services.LogStorageService;
import com.openhouse.logs.utils.HttpServletRequestUtil;
import com.openhouse.logs.utils.ObjectParserUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.openhouse.logs.entity.Action;

/**
 * @author tan3
 * @ClassName LogController.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 22 - 06 : 02
 */
@RestController
public class LogController {


    @Autowired
    private LogStorageService logStorageService;

    @Autowired
    private LogRetrieveService logRetrieveService;


    @PostMapping(value = "/send_posts")
    @ResponseBody
    public JsonResult saveLogs(@RequestBody LogVO logVO) {
        User user = null;
        List<Action> actionList = null;
        Map<Class, List<Property>> propertyMap = new HashMap<>();

        try {
            user = ObjectParserUtil.getUserFromLogVO(logVO);
        } catch (InvalidInputException exception) {
            return JsonResult.errorMsg("Invalid Input Error, Details:  \n " + exception.getMessage());
        }

        try {
            actionList = ObjectParserUtil.getActionsFromLogVO(logVO);
        } catch (InvalidInputException exception) {
            return JsonResult.errorMsg("Invalid Input Error, Details: \n " + exception.getMessage());
        }


        try {
            propertyMap = ObjectParserUtil.getpropertyMapFromLogVO(logVO, actionList);
        } catch (InvalidInputException exception) {
            return JsonResult.errorMsg("Invalid Input Error, Details: \n " + exception.getMessage());
        }

        try {
            logStorageService.storeUserLog(user, actionList, propertyMap);
        } catch (LogServiceException exception) {
            return JsonResult.errorMsg("Invalid Log Service Insertion Error, Details: \n "
                + exception.getMessage());
        }

        return JsonResult.success();
    }


    @GetMapping(value = "/retrieve_logs/")
    @ResponseBody
    public JsonResult saveLogs(HttpServletRequest httpServletRequest) {
        String userId =  HttpServletRequestUtil.getUserIdFromHttpRequest(httpServletRequest);
        Date[] dates = null;
        ActionType actionType = null;

        try {
            dates = HttpServletRequestUtil.getTimeRange(httpServletRequest);
        } catch (InvalidInputException exception) {
            return JsonResult.errorMsg("Invalid InputParameter Error, Details: \n " + exception.getMessage());
        }

        try {
            actionType = HttpServletRequestUtil.getActionTypeEnum(httpServletRequest);
        } catch (InvalidInputException exception) {
            return JsonResult.errorMsg("Invalid InputParameter Error, Details: \n " + exception.getMessage());
        }

        // no input parameters are given
        if (userId == null && dates == null && actionType == null) {
            return JsonResult.errorMsg("No searching condition provided.");
        }

        // Step 3: (Paralleled) query base on the conditions.

        // add future tasks.
        List<CompletableFuture<Map<String, ActionVO>>> futures = new ArrayList<>();
        if (userId != null) {
            futures.add(logRetrieveService.fetchLogByUserId(userId));
        }

        if (dates != null) {
            futures.add(logRetrieveService.fetchLogByTimeRange(dates[0], dates[1]));
        }

        if (actionType != null) {
            futures.add(logRetrieveService.fetchLogByLogType(actionType));
        }

        CompletableFuture[] cfs = futures.toArray(new CompletableFuture[futures.size()]);

        // combination of all completable future and wait until all of them are finished
        try {
            CompletableFuture allof = CompletableFuture.allOf(cfs);
            allof.join();
        } catch (Exception e) {
            return JsonResult.errorMsg("Query Error[or Interruption Error], details: \n " + e.getMessage());
        }

        List<Map<String, ActionVO>> mapList = new ArrayList<>();

        // iterate the future result.
        for(CompletableFuture<Map<String, ActionVO>> completableFuture: futures) {
            try {
                mapList.add(completableFuture.get());
            } catch (Exception e) {
                return JsonResult.errorMsg("Threads Interception/Execution Exception, details: \n " + e.getMessage());
            }
        }
        Map<String, ActionVO>  resultMap = intersectionOfKMaps(mapList);

        return JsonResult.success(new ArrayList<>(resultMap.values()));
    }


    private Map<String,ActionVO> intersectionOfKMaps(List<Map<String, ActionVO>> mapList) {
        int size = mapList.size();
        Map<String, ActionVO> resultMap = mapList.get(0);
        for (int idx = 1 ; idx < size ; idx++) {
            resultMap = intersectionOfTwoMaps(resultMap, mapList.get(1));
        }
        return resultMap;
    }


    private Map<String, ActionVO> intersectionOfTwoMaps(Map<String, ActionVO> map1,
        Map<String, ActionVO> map2) {
        Map<String, ActionVO> intersection = new HashMap();
        for (String id: map1.keySet())
        {
            if (map2.containsKey(id)) {
                intersection.put(id, map1.get(id));
            }
        }
        return intersection;
    }
}
