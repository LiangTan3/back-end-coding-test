package com.openhouse.logs.exceptions;

import java.io.Serializable;

/**
 * @author tan3
 * @ClassName InvalidInputException.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 22 - 07 : 50
 */
public class InvalidInputException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = 3220280971107559630L;


    public InvalidInputException() {
    }

    public InvalidInputException(String message) {
        super(message);
    }
}
