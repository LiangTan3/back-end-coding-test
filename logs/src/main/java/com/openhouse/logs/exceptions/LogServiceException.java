package com.openhouse.logs.exceptions;

import java.io.Serializable;

/**
 * @author tan3
 * @ClassName LogServiceException.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 22 - 09 : 02
 */
public class LogServiceException  extends RuntimeException implements Serializable {

    private static final long serialVersionUID = 7820672814878705828L;

    public LogServiceException() {
    }

    public LogServiceException(String message) {
        super(message);
    }
}
