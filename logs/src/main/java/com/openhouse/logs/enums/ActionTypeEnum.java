package com.openhouse.logs.enums;

import com.openhouse.logs.entity.Click;
import com.openhouse.logs.entity.Navigate;
import com.openhouse.logs.entity.View;
import java.util.HashSet;
import java.util.Set;

/**
 * @author tan3
 * @ClassName ActionTypeEnum.java
 * @Description Enum class mapping to action_type_tb. including the class type information
 * @createTime 2020 -  08 - 22 - 07 : 16
 */
public enum ActionTypeEnum {

    CLICK(0, "CLICK", Click.class),
    VIEW(1, "VIEW", View.class),
    NAVIGATE(2, "NAVIGATE", Navigate.class);

    private int actionTypeId;

    private String actionTypeName;

    private Class classInfo;

    ActionTypeEnum(int actionTypeId, String actionTypeName, Class classInfo) {
        this.actionTypeId = actionTypeId;
        this.actionTypeName = actionTypeName;
        this.classInfo = classInfo;
    }

    public int getActionTypeId() {
        return actionTypeId;
    }


    public String getActionTypeName() {
        return actionTypeName;
    }

    public Class getClassInfo() {
        return classInfo;
    }


    public static int getActionTypeEnumIdByName(String actionTypeName) {
        for (ActionTypeEnum actionTypeEnum: ActionTypeEnum.values()) {
            if (actionTypeEnum.getActionTypeName().equals(actionTypeName)) {
                return actionTypeEnum.getActionTypeId();
            }
        }
        return -1;
    }

    public static String getActionTypeEnumNameById(int actionTypeId) {
        for (ActionTypeEnum actionTypeEnum: ActionTypeEnum.values()) {
            if (actionTypeEnum.getActionTypeId() == actionTypeId) {
                return actionTypeEnum.getActionTypeName();
            }
        }
        return null;
    }

    public static Set<Class> getAllActionTypeClass() {
        Set<Class>  typeSet = new HashSet<>();

        for (ActionTypeEnum actionTypeEnum: ActionTypeEnum.values()) {
            typeSet.add(actionTypeEnum.getClassInfo());
        }
        return typeSet;
    }

}
