package com.openhouse.logs.repository;

import static org.junit.Assert.*;

import com.openhouse.logs.LogsApplicationTests;
import com.openhouse.logs.entity.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author tan3
 * @ClassName UserRepositoryTest.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 19 : 46
 */
public class UserRepositoryTest extends LogsApplicationTests {


    @Autowired
    private UserRepository userRepository;


    @Test
    public void insertAndQueryUser() {
        // Given
        String userId = "ABC123XYZ";
        String sessionId = "XYZ456ABC";
        User user = User.builder()
            .userId(userId)
            .sessionId(sessionId)
            .build();


        // When
        int affectedRow = userRepository.insertUser(user);
        assertEquals(affectedRow, 1);
        User queryUser = userRepository.queryUserByUserId("ABC123XYZ");

        // Then, make sure the user we insert is what we have.
        assertEquals(queryUser.getUserId(), "ABC123XYZ");
        assertEquals(queryUser.getSessionId(), "XYZ456ABC");

        // clearance to make the test repeatable (test loop)
        userRepository.deleteUserByUserId(userId);

    }
}
