package com.openhouse.logs.repository;

import static org.junit.Assert.*;

import com.openhouse.logs.LogsApplicationTests;
import com.openhouse.logs.entity.Click;
import com.openhouse.logs.utils.SnowflakeIdWorker;
import java.util.ArrayList;
import java.util.List;
import org.junit.Ignore;
import org.junit.Test;
import com.openhouse.logs.entity.Action;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author tan3
 * @ClassName ClickRepositoryTest.java
 * @Description This test can only run under the condition that
 * there is no foreign key constraints on action_id
 * if there exists, please drop the key before run the test
 * @createTime 2020 -  08 - 21 - 22 : 06
 */
public class ClickRepositoryTest extends LogsApplicationTests {

    @Autowired
    private ClickRepository clickRepository;

    private static final int TEST_CASES = 10;

    private static SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0,0);

    @Test
    @Ignore
    public void ClickTestLoop() {
        List<Action> actions = new ArrayList<>();
        List<Click> clicks = new ArrayList<>();

        // 1. prepare the data
        for (int idx = 0; idx < TEST_CASES; idx++) {
            String actionId = String.valueOf(idWorker.nextId());
            Action action = Action
                .builder()
                .actionId(actionId)
                .build();
            actions.add(action);
        }

        for (int idx = 0 ; idx < TEST_CASES; idx ++) {
            Click click = Click.builder()
                .clickId(String.valueOf(idWorker.nextId()))
                .locationX(idx)
                .locationY(idx)
                .action(actions.get(idx))
                .build();
            clicks.add(click);
        }

        // 2. Test batch insert and query
        int affectedRows = clickRepository.batchInsertClick(clicks);
        assertEquals(affectedRows, TEST_CASES);

        for (int idx = 0; idx < TEST_CASES; idx++) {
            String id = actions.get(idx).getActionId();
            Click click = clickRepository.queryClicksByActionId(id);
            assertEquals(click.getClickId(),
                clicks.get(idx).getClickId());
        }

        // 3. delete the clicks
        for (int idx = 0 ; idx < TEST_CASES; idx++) {
            clickRepository.deleteClickByID(clicks.get(idx).getClickId());
        }
    }
}
