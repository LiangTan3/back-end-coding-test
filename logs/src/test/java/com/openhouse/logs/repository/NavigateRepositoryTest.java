package com.openhouse.logs.repository;

import static org.junit.Assert.*;

import com.openhouse.logs.LogsApplicationTests;
import com.openhouse.logs.entity.Action;
import com.openhouse.logs.entity.Navigate;
import com.openhouse.logs.utils.SnowflakeIdWorker;
import java.util.ArrayList;
import java.util.List;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author tan3
 * @ClassName NavigateRepositoryTest.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 23 : 07
 */
public class NavigateRepositoryTest  extends LogsApplicationTests {

    @Autowired
    private NavigateRepository navigateRepository;
    private static final int TEST_CASES = 10;

    private static SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0,0);

    @Test
    @Ignore
    public void navigateTestLoop() {
        List<Action> actions = new ArrayList<>();
        List<Navigate> navigates = new ArrayList<>();

        // 1. prepare the data
        for (int idx = 0; idx < TEST_CASES; idx++) {
            String actionId = String.valueOf(idWorker.nextId());
            Action action = Action
                .builder()
                .actionId(actionId)
                .build();
            actions.add(action);
        }

        for (int idx = 0 ; idx < TEST_CASES; idx ++) {
            Navigate navigate = Navigate.builder()
                .navigateId(String.valueOf(idWorker.nextId()))
                .action(actions.get(idx))
                .pageFrom("from")
                .pageTo("to")
                .build();
            navigates.add(navigate);
        }

        // 2. Test batch insert and query
        int affectedRows = navigateRepository.batchInsertNavigate(navigates);
        assertEquals(affectedRows, TEST_CASES);

        for (int idx = 0; idx < TEST_CASES; idx++) {
            String id = actions.get(idx).getActionId();
            Navigate navigate = navigateRepository.queryNavigateByActionId(id);
            assertEquals(navigate.getNavigateId(),
                navigates.get(idx).getNavigateId());
        }

        // 3. delete the views
        for (int idx = 0 ; idx < TEST_CASES; idx++) {
            navigateRepository.deleteNavigateByID(
                navigates.get(idx).getNavigateId());
        }
    }
}
