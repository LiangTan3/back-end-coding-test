package com.openhouse.logs.repository;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.openhouse.logs.LogsApplicationTests;
import com.openhouse.logs.entity.ActionType;
import com.openhouse.logs.entity.User;
import com.openhouse.logs.utils.SnowflakeIdWorker;
import com.openhouse.logs.utils.TimeUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.openhouse.logs.entity.Action;
/**
 * @author tan3
 * @ClassName ActionRepositoryTest.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 19 : 22
 */
public class ActionRepositoryTest extends LogsApplicationTests {

    @Autowired
    private ActionRepository actionRepository;
    @Autowired
    private UserRepository userRepository;

    private static SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0,0);

    /**
     * Test batch insert actions,
     * query actions by action id  and delete actions
     * within a test loop
     */
    @Test
    public void ActionsTestLoop() {
        List<Action> actions = new ArrayList<>();


        // 1. Given user, actionType, and action.
        String userId = "ABC123XYZ";
        String sessionId = "XYZ456ABC";
        User user = User.builder()
            .userId(userId)
            .sessionId(sessionId)
            .build();

        int actionTypeId = 0;
        String actionTypeName = "CLICK";
        ActionType aCtionType = ActionType
            .builder()
            .actionTypeId(actionTypeId)
            .actionTypeName(actionTypeName)
            .build();

        String timeStr = "2018-10-18T21:37:28-06:00";
        Date date = TimeUtil.parseOffsetTimeStr(timeStr);
        String actionId = String.valueOf(idWorker.nextId());
        Action action = Action.builder()
            .actionId(actionId)
            .actionTime(date)
            .user(user)
            .actionType(aCtionType).build();

        actions.add(action);

        // When : insert users into the databases.
        userRepository.insertUser(user);
        int affectRows = actionRepository.batchInsertActions(actions);


        // Then : make sure the data are insert successfully
        assertTrue("actions are insert into "
            + "the MySQL successfully",
            affectRows == actions.size());



        // make sure the actions we insert is the same as what we have

        // 1. query by userId
        List<Action> queryActionList = actionRepository.queryActionsByUserId(userId);
        assertEquals(queryActionList.size(), actions.size());
        Action resultAction = queryActionList.get(0);

        assertEquals(resultAction.getActionId(), actionId);
        assertEquals(resultAction.getActionType().getActionTypeId()
            , actionTypeId);
        assertEquals(resultAction.getActionTime(), date);
        assertEquals(resultAction.getUser().getSessionId(), sessionId);

        // 2. query by log Type
        queryActionList = actionRepository.queryActionsByActionTypeId(1);
        assertEquals(queryActionList.size(), 0);

        // 3. query by time range
        String fromStr = "2018-10-18T21:35:28-06:00";
        String toStr = "2018-10-18T21:38:28-06:00";
        Date fromDate = TimeUtil.parseOffsetTimeStr(fromStr);
        Date toDate = TimeUtil.parseOffsetTimeStr(toStr);
        queryActionList = actionRepository.queryActionsByTimeRange(fromDate, toDate);

        assertEquals(queryActionList.size(), actions.size());



        // delete the actions and user we added to the databases
        actionRepository.deleteActionByActionId(actionId);
        userRepository.deleteUserByUserId(userId);

    }
}
