package com.openhouse.logs.repository;

import static org.junit.Assert.*;

import com.openhouse.logs.LogsApplicationTests;
import com.openhouse.logs.entity.Action;
import com.openhouse.logs.entity.Click;
import com.openhouse.logs.entity.View;
import com.openhouse.logs.utils.SnowflakeIdWorker;
import java.util.ArrayList;
import java.util.List;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.result.ViewResultMatchers;

/**
 * @author tan3
 * @ClassName ViewRepositoryTest.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 21 - 22 : 48
 */
public class ViewRepositoryTest extends LogsApplicationTests {


    @Autowired
    private ViewRepository viewRepository;
    private static final int TEST_CASES = 10;

    private static SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0,0);

    @Test
    @Ignore
    public void ViewTestLoop() {
        List<Action> actions = new ArrayList<>();
        List<View> views = new ArrayList<>();

        // 1. prepare the data
        for (int idx = 0; idx < TEST_CASES; idx++) {
            String actionId = String.valueOf(idWorker.nextId());
            Action action = Action
                .builder()
                .actionId(actionId)
                .build();
            actions.add(action);
        }

        for (int idx = 0 ; idx < TEST_CASES; idx ++) {
            View view = View.builder()
                .viewId(String.valueOf(idWorker.nextId()))
                .viewedId("testViewedId")
                .action(actions.get(idx))
                .build();
            views.add(view);
        }

        // 2. Test batch insert and query
        int affectedRows = viewRepository.batchInsertView(views);
        assertEquals(affectedRows, TEST_CASES);

        for (int idx = 0; idx < TEST_CASES; idx++) {
            String id = actions.get(idx).getActionId();
            View view = viewRepository.queryViewByActionId(id);
            assertEquals(view.getViewId(),
                views.get(idx).getViewId());
        }

        // 3. delete the views
        for (int idx = 0 ; idx < TEST_CASES; idx++) {
            viewRepository.deleteViewByID(views.get(idx).getViewId());
        }
    }
}
