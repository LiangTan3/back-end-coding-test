package com.openhouse.logs.services.impl;

import static org.junit.Assert.*;

import com.openhouse.logs.LogsApplicationTests;
import com.openhouse.logs.entity.Action;
import com.openhouse.logs.entity.ActionType;
import com.openhouse.logs.entity.Click;
import com.openhouse.logs.entity.Navigate;
import com.openhouse.logs.entity.Property;
import com.openhouse.logs.entity.User;
import com.openhouse.logs.entity.View;
import com.openhouse.logs.repository.ActionRepository;
import com.openhouse.logs.repository.ClickRepository;
import com.openhouse.logs.repository.NavigateRepository;
import com.openhouse.logs.repository.UserRepository;
import com.openhouse.logs.repository.ViewRepository;
import com.openhouse.logs.services.LogStorageService;
import com.openhouse.logs.utils.SnowflakeIdWorker;
import com.openhouse.logs.utils.TimeUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author tan3
 * @ClassName LogStorageServiceImplTest.java
 * @Description Please enter description here
 * @createTime 2020 -  08 - 22 - 09 : 33
 */
public class LogStorageServiceImplTest extends LogsApplicationTests{

    @Autowired
    private LogStorageService logStorageService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ActionRepository actionRepository;

    @Autowired
    private NavigateRepository navigateRepository;
    @Autowired
    private ViewRepository viewRepository;
    @Autowired
    private ClickRepository clickRepository;

    private static SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0,0);

    @Test
    public void storeUserLogTestLoop() {

        // 1. Given user, actionType, and action.

        // generate user
        String userId = "ABC123XYZ";
        String sessionId = "XYZ456ABC";
        User user = User.builder()
            .userId(userId)
            .sessionId(sessionId)
            .build();

        // generate actions (For simplicity, we can assume that those actions share the same action)
        int actionTypeId = 0;
        String actionTypeName = "CLICK";
        ActionType aCtionType = ActionType
            .builder()
            .actionTypeId(actionTypeId)
            .actionTypeName(actionTypeName)
            .build();

        String timeStr = "2018-10-18T21:37:28-06:00";
        Date date = TimeUtil.parseOffsetTimeStr(timeStr);
        String actionId = String.valueOf(idWorker.nextId());
        Action action = Action.builder()
            .actionId(actionId)
            .actionTime(date)
            .user(user)
            .actionType(aCtionType).build();
        List<Action> actions = new ArrayList<>();
        actions.add(action);

        // generate actions properties CLICK, VIEW, AND  NAVIGATES
        Map<Class, List<Property>> propertyMap = new HashMap<>();

        List<Property> clicks = new ArrayList<>();
        Click click = Click.builder()
            .clickId(String.valueOf(idWorker.nextId()))
            .locationX(52)
            .locationY(11)
            .action(actions.get(0))
            .build();
        clicks.add(click);
        propertyMap.put(Click.class, clicks);

        List<Property> navigates = new ArrayList<>();
        Navigate navigate = Navigate.builder()
            .navigateId(String.valueOf(idWorker.nextId()))
            .action(actions.get(0))
            .pageFrom("communities")
            .pageTo("inventory")
            .build();
        navigates.add(navigate);
        propertyMap.put(Navigate.class, navigates);

        List<Property> views = new ArrayList<>();
        View view = View.builder()
            .viewId(String.valueOf(idWorker.nextId()))
            .action(actions.get(0))
            .viewedId("FDJKLHSLD")
            .build();
        views.add(view);
        propertyMap.put(View.class, views);

        //2. Insert into Database:
        int code = logStorageService.storeUserLog(user, actions, propertyMap);
        assertEquals(code, 0);


        //3. delete
        clickRepository.deleteClickByID(click.getClickId());
        viewRepository.deleteViewByID(view.getViewId());
        navigateRepository.deleteNavigateByID(navigate.getNavigateId());
        actionRepository.deleteActionByActionId(action.getActionId());
        userRepository.deleteUserByUserId(user.getUserId());

    }

}
