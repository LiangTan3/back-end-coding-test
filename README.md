# back-end-coding-test

Back End Coding test - OpenHouse.AI 



### Dependencies:

- Java 8
- Maven
- MySQL 8.0

### BUILD:  

Before starting the springboot application , make sure that MySQL service is running. 

If you want to compile but not run tests:

```
cd logs
mvn clean install -DskipTests
```


### Deploy : 

 I deploy the service(SpringBoot jar files) on AliCloud with Elastic Compute Service. It is a linux server with CentOS 6.9 and the script for Nginx is as follows:

<div>
  <img src="./img/img4.png" alt="fig_4" width="90%" height="90%">
</div>


### RestAPI: 

1. POST user logs, here is the sample call with sample data:

<div>
  <img src="./img/img1.png" alt="fig_4" width="90%" height="90%">
</div>


2. GET user logs, examples are as follows:

-   http://47.88.32.242:12001/api/retrieve_logs/?userId=&logType=CLICK&timeFrom=2018-10-18T21:37:28-06:00&timeTo=2018-10-18T21:37:31-06:00

```json
{
  status: 200,
  data: [
    {
      time: "2018-10-18T21:37:28-06:00",
      type: "CLICK",
      properties: {
        clickId: "85022438215122944",
        locationX: 52,
        locationY: 11
      }
    }
  ],
  success: "true"
}

```

  - http://47.88.32.242:12001/api/retrieve_logs/?userId=&logType=&timeFrom=2018-10-18T21:37:28-06:00&timeTo=2018-10-18T21:37:31-06:00

```json
{
  status: 200,
  data: [
    {
      time: "2018-10-18T21:37:30-06:00",
      type: "NAVIGATE",
      properties: {
        navigateId: "85022438265454592",
        pageFrom: "communities",
        pageTo: "inventory"
      }
    },
  {
    time: "2018-10-18T21:37:30-06:00",
    type: "VIEW",
    properties: {
      viewId: "85022438248677376",
      viewedId: "FDJKLHSLD"
    }
  },
  {
    time: "2018-10-18T21:37:28-06:00",
    type: "CLICK",
    properties: {
      clickId: "85022438215122944",
      locationX: 52,
      locationY: 11
    }
  }
  ],
  success: "true"
}
```

### Some bad cases: 

- Invalid Time Input :

  http://47.88.32.242:12001/api/retrieve_logs/?userId=&logType=&timeFrom=Z2018-10-18Z21:37:28-06:00&timeTo=2018-10-18T21:37:31-06:00

  ```json
  {
    status: 400,
    error: "Invalid InputParameter Error, Details: action [action time format] is invalid, details: Text 'Z2018-10-18Z21:37:28-06:00' could not be parsed at index 0"
  }
  ```

- Invalid Log Type: 
  http://47.88.32.242:12001/api/retrieve_logs/?userId=&logType=JUMP&timeFrom=2018-10-18T21:37:28-06:00&timeTo=2018-10-18T21:37:31-06:00

  ```json
  {
    status: 400,
    error: "Invalid InputParameter Error, Details: logType is invalid"
  } 
  ```

- Missing Searching condition:
  http://47.88.32.242:12001/api/retrieve_logs/?userId=&logType=&timeFrom

  ```json
  {
    status: 400,
    error: "No searching condition provided."
  }
  ```

- and so on .... 


### Database Design 

The schema for ```user_log_db``` is written in the file ```user_log_db.sql```. 

The E-R diagram is as follows(produced by JetBrains DataGrip)

<div>
  <img src="./img/db.png" alt="fig_db" width="90%" height="90%">
</div>


### Follow UP:

To make it cloud-scalable, I think there are several steps which can be considered: 

1.  Nginx can be viewed as the Reverse Proxy of back-end servers,
we can perform consistent hashing  based on the Income Request IP address,therefore to achieve load balancing.

2. If the back-end server still cannot affort the network traffic, we can add a MessageQueue to buffer the input traffic to gurantee that no packages will lose. 

3. Query occurs much more ofen than Write, So, we can have multiple MySQL server in Master-Slave pattern. Only one MySQL server is responsible for writing. 

4. We can store some data which are frequently serched in the in-memory database, such as Redis.
   
5. Backup the old data (rarely queried) to File System.

6. In Distributed System, the consistency factor should be guranteed, such as there is not confiction on UUID. For simplicity, I use Twitter SnowFlake Algorithm for generating id, In a real production enviroment, ZooKeeper is usually used for this purpose.

















